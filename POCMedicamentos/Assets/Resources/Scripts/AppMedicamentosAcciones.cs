﻿using Neoris.RA.Itween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Vuforia;

using UnityEngine.Video;

public class AppMedicamentosAcciones : MonoBehaviour
{
    private GameObject VideoGameObject;
    private GameObject ProspectoGameObject;

    // Start is called before the first frame update
    void Start()
    {
        VideoGameObject = GameObject.Find("Video");
        ProspectoGameObject = GameObject.Find("Prospecto");
    }

    // Update is called once per frame
    void Update()
    { 
        if (Input.GetMouseButtonDown(0))
        {

            //obtenemos la posición del click
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "VideoPreview")
                {
                    if(VideoGameObject == null)
                    {
                        Debug.LogError("ta nulo");
                    }
                    VideoGameObject.GetComponent<MultimediaContainer>().StartContent();
                }
                if (hit.transform.name == "ProspectoPreview")
                {
                    ProspectoGameObject.GetComponent<MultimediaContainer>().StartContent();
                }
                if (hit.transform.name == "comprar")
                {
                    Application.OpenURL("https://tienda.farmashop.com.uy/actron-600mg-rapida-accion-10-capsulas.html"); ;
                }

            }
        }
    }

    //public void OnTrackableTargetChanged()
    //{
    //    var videoPlayer = VideoGameObject.GetComponent<VideoPlayer>();

    //    if (VideoGameObject.GetComponentsInChildren<Renderer>(true)[0].enabled)
    //    {
    //        // Play video when target is found

    //        videoPlayer.Play();
    //    }
    //    else
    //    {
    //        // Stop video when target is lost
    //        videoPlayer.Stop();
    //    }
    //}
}
