﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Neoris.RA.Itween
{
    public static class ItweenInteractions
    {
        #region Methods

        public static void ScaleGameObject(GameObject from, GameObject to)
        {
            iTween.ScaleTo(from, iTween.Hash("scale", to.transform.localScale, "time", 0.0f));
        }

        public static void ScaleGameObject(GameObject from, Vector3 to)
        {
            iTween.ScaleTo(from, iTween.Hash("scale", to));
        }

        public static void ScaleGameObjectInTime(GameObject from, GameObject to, float time)
        {
            iTween.ScaleTo(from, iTween.Hash("scale", to.transform.localScale, "time", time, "easetype", "easeInQuad"));
        }

        public static void ScaleGameObjectInTime(GameObject from, Vector3 to, float time)
        {
            iTween.ScaleTo(from, iTween.Hash("scale", to, "time", time, "easetype", "spring"));
        }

        public static void ScaleGameObjectInTimeWithDelay(GameObject from, GameObject to, float time, float delay)
        {
            iTween.ScaleTo(from, iTween.Hash("scale", to.transform.localScale, "time", time, "delay", delay, "easetype", "spring"));
        }

        public static void ScaleGameObjectInTimeWithDelay(GameObject from, Vector3 to, float time, float delay)
        {
            iTween.ScaleTo(from, iTween.Hash("scale", to, "time", time, "delay", delay, "easetype", "spring"));
        }

        public static void MoveGameObject(GameObject from, GameObject to)
        {
            iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false));
        }

        public static void MoveGameObjectInTime(GameObject from, GameObject to, float time)
        {
            iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "easetype", "spring"));
        }

        public static void MoveGameObjectInTimeWithDelay(GameObject from, GameObject to, float time, float delay)
        {
            iTween.MoveTo(from, iTween.Hash("position", to.transform.position, "islocal", false, "time", time, "delay", delay, "easetype", "spring"));
        }       

        #endregion
    }
}
