﻿using Neoris.RA.Itween;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class VideoMedicamento : MultimediaContainer
{
    // Start is called before the first frame update
    void Start()
    {
        Init(); 
    } 

    public override void StartContent()
    {
        ItweenInteractions.ScaleGameObject(PreviewObject, Vector3.zero);
        ItweenInteractions.ScaleGameObjectInTimeWithDelay(UIContainer, Vector3.one, 1.0f, 1.0f); 
        FullScreenObject.GetComponent<RawImageVideoPlayer>().PlayContent();
    }

    public override void EndContent()
    {
        ItweenInteractions.ScaleGameObjectInTime(UIContainer, Vector3.zero, 1.0f);
        ItweenInteractions.ScaleGameObjectInTimeWithDelay(PreviewObject, OriginalScale, 1.0f, 1.0f); 
        FullScreenObject.GetComponent<RawImageVideoPlayer>().StopContent();
    } 
}
