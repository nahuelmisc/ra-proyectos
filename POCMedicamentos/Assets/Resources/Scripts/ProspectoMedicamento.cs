﻿using Neoris.RA.Itween;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProspectoMedicamento : MultimediaContainer
{
    // Start is called before the first frame update
    void Start()
    {
        Init();

        FullScreenObject.GetComponent<Image>().enabled = false;
    }

    public override void StartContent()
    {
        FullScreenObject.GetComponent<Image>().enabled = true;
        ItweenInteractions.ScaleGameObject(PreviewObject, Vector3.zero);
        ItweenInteractions.ScaleGameObjectInTimeWithDelay(UIContainer, Vector3.one, 1.0f, 1.0f);
    }

    public override void EndContent()
    {
        ItweenInteractions.ScaleGameObjectInTime(UIContainer, Vector3.zero, 1.0f);
        ItweenInteractions.ScaleGameObjectInTimeWithDelay(PreviewObject, OriginalScale, 1.0f, 1.0f);
        FullScreenObject.GetComponent<Image>().enabled = false;
    } 
}
