﻿using Neoris.RA.Itween;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public abstract class MultimediaContainer : MonoBehaviour
{
    public GameObject FullScreenObject;
    public GameObject PreviewObject;
    public GameObject UIContainer;
    protected Vector3 OriginalScale;

    public virtual void Init()
    {
        OriginalScale = PreviewObject.transform.localScale;

        ItweenInteractions.ScaleGameObject(UIContainer, Vector3.zero);

        UIContainer.GetComponent<Button>().onClick.AddListener(() => EndContent());
    }

    public abstract void StartContent();
    public abstract void EndContent();
}

